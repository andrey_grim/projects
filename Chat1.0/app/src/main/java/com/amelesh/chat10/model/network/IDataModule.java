package com.amelesh.chat10.model.network;

import com.amelesh.chat10.model.dto.ShortConversationDto;
import com.amelesh.chat10.util.Consumer;

import java.util.List;

/**
 * Created by UserPC on 27.02.2017.
 */

public interface IDataModule {

    void getConversationList(int offset, int limit, Consumer<List<ShortConversationDto>> callback);
}
