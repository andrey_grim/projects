package com.amelesh.chat10;

import android.app.Application;

import com.amelesh.chat10.model.AppComponent;
import com.amelesh.chat10.model.DaggerAppComponent;
import com.amelesh.chat10.model.base.BaseProvider;

/**
 * Created by UserPC on 26.02.2017.
 */

public class AppContext extends Application {

    protected AppComponent appComponent;

    @Override
    public void onCreate() {
        super.onCreate();
        appComponent = DaggerAppComponent
                .builder()
                .baseProvider(new BaseProvider(this))
                .build();
        getAppComponent().inject(this);
    }

    public AppComponent getAppComponent() {
        return appComponent;
    }
}
