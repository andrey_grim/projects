package com.amelesh.chat10.model.dto;

import java.util.List;

/**
 * Created by UserPC on 27.02.2017.
 */

public class ShortConversationDto {

    private String conversationUserName;
    private String userImage;
    private List<ShortMessageDto> shortMessageDto;

    public String getConversationUserName() {
        return conversationUserName;
    }

    public void setConversationUserName(String conversationUserName) {
        this.conversationUserName = conversationUserName;
    }

    public String getUserImage() {
        return userImage;
    }

    public void setUserImage(String userImage) {
        this.userImage = userImage;
    }

    public List<ShortMessageDto> getShortMessageDto() {
        return shortMessageDto;
    }

    public void setShortMessageDto(List<ShortMessageDto> shortMessageDto) {
        this.shortMessageDto = shortMessageDto;
    }
}
