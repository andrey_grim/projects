package com.amelesh.chat10.model.network;

import android.os.SystemClock;

import com.amelesh.chat10.model.AppComponent;
import com.amelesh.chat10.model.dto.ShortConversationDto;
import com.amelesh.chat10.model.dto.ShortMessageDto;
import com.amelesh.chat10.model.dto.ShortUserDto;
import com.amelesh.chat10.model.dto.enums.MessageState;
import com.amelesh.chat10.util.Consumer;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.UUID;




/**
 * Created by UserPC on 27.02.2017.
 */

public class DataModuleDemo implements IDataModule {

    private AppComponent appComponent;

    public DataModuleDemo(AppComponent appComponent) {
        this.appComponent = appComponent;
        appComponent.inject(this);
    }

    @Override
    public void getConversationList(int offset, int limit, Consumer<List<ShortConversationDto>> callback) {

        List<ShortConversationDto> shortConversationDtoList = new ArrayList<>();

        for (int i = offset; i < offset + limit; i++) {

            ShortUserDto shortUserDto = new ShortUserDto();
            shortUserDto.setName("Andrey");
            shortUserDto.setUuid(UUID.randomUUID().toString());

            ShortMessageDto shortMessageDto = new ShortMessageDto();
            shortMessageDto.setDate(SystemClock.currentThreadTimeMillis());
            shortMessageDto.setMessage("dsfgsdfg" + i);
            shortMessageDto.setMessageState(MessageState.UNREAD);
            shortMessageDto.setSender(shortUserDto);

            ShortConversationDto conversationDto = new ShortConversationDto();
            conversationDto.setConversationUserName("User" + i);
            conversationDto.setShortMessageDto(Arrays.asList(shortMessageDto));

            shortConversationDtoList.add(conversationDto);
        }

        callback.accept(shortConversationDtoList);
    }


}
