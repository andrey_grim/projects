package com.amelesh.chat10.constants;

/**
 * Created by UserPC on 05.02.2017.
 */

public interface Constants {

    String LAYOUT_ID = "layout_id";
    String LAYOUT_RES = "layout_res";
    String FRAGMENT_NAME = "fragment_name";
}
