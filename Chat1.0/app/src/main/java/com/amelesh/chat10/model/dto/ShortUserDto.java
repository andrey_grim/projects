package com.amelesh.chat10.model.dto;

/**
 * Created by UserPC on 27.02.2017.
 */

public class ShortUserDto {

    private String name;
    private String uuid;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }
}
