package com.amelesh.chat10.model;

import com.amelesh.chat10.AppContext;
import com.amelesh.chat10.model.base.BaseProvider;
import com.amelesh.chat10.model.network.DataModuleDemo;

import javax.inject.Singleton;

import dagger.Component;

/**
 * Created by UserPC on 26.02.2017.
 */

@Singleton
@Component(modules = {BaseProvider.class})
public interface AppComponent {


    void inject(AppContext appContext);

    void inject(DataModuleDemo dataModule);
}
