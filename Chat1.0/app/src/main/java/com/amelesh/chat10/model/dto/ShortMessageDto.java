package com.amelesh.chat10.model.dto;

import com.amelesh.chat10.model.dto.enums.MessageState;

/**
 * Created by UserPC on 27.02.2017.
 */

public class ShortMessageDto {

    private ShortUserDto sender;
    private long date;
    private String message;
    private MessageState messageState;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public MessageState getMessageState() {
        return messageState;
    }

    public void setMessageState(MessageState messageState) {
        this.messageState = messageState;
    }

    public ShortUserDto getSender() {
        return sender;
    }

    public void setSender(ShortUserDto sender) {
        this.sender = sender;
    }

    public long getDate() {
        return date;
    }

    public void setDate(long date) {
        this.date = date;
    }
}
