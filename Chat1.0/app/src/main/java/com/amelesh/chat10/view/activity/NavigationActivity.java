package com.amelesh.chat10.view.activity;

import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;

import com.amelesh.chat10.R;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by UserPC on 22.02.2017.
 */

public class NavigationActivity extends BaseAppCompatActivity {

    @BindView(R.id.drawerLayout)
    protected DrawerLayout mDrawer;
    @BindView(R.id.toolBarAppBar)
    protected Toolbar mToolbar;
    @BindView(R.id.navigationView)
    protected NavigationView mNavView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_navigation);

        ButterKnife.bind(this);
        mNavView.setNavigationItemSelectedListener(item -> {
            Fragment fragment = null;
            Class fragmentClass;

            switch (item.getItemId()){
                case R.id.itemGroupChat:
                    break;
                case R.id.itemSettings:
                    break;
                case R.id.itemFAQ:
                    break;
            }
            return true;
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            mDrawer.openDrawer(GravityCompat.START);
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
