package com.amelesh.chat10.view.widgets;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.amelesh.chat10.R;

import butterknife.BindView;

/**
 * Created by UserPC on 27.02.2017.
 */

public class ConversationView extends LinearLayout {

    @BindView(R.id.imageViewConversationLayout)
    protected ImageView imageViewAva;
    @BindView(R.id.imageViewIsReadConversationLayout)
    protected ImageView imageViewIsRead;
    @BindView(R.id.textViewDateConversationLayout)
    protected TextView textViewDate;
    @BindView(R.id.textViewMessagesConversationLayout)
    protected TextView textViewMessagees;
    @BindView(R.id.textViewNameConversationLayout)
    protected TextView textViewName;

    public ConversationView(Context context) {
        super(context);
    }

    public ConversationView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public ConversationView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    public ConversationView(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
    }
}
