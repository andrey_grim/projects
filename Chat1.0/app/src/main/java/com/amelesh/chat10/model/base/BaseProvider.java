package com.amelesh.chat10.model.base;

import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import com.amelesh.chat10.AppContext;
import com.amelesh.chat10.Config;
import com.amelesh.chat10.model.network.IApiModule;
import com.google.gson.FieldNamingPolicy;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.jakewharton.retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import okhttp3.Cache;
import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by UserPC on 26.02.2017.
 */

@Module
public class BaseProvider {

    private AppContext appContext;

    public BaseProvider(AppContext appContext) {
        this.appContext = appContext;
    }

    @Provides
    @Singleton
    SharedPreferences sharedPreferences() {
        return PreferenceManager.getDefaultSharedPreferences(appContext);
    }

    @Provides
    @Singleton
    Cache cache() {
        return new Cache(appContext.getCacheDir(), Config.CACH_SIZE);
    }

    @Provides
    @Singleton
    Gson gson() {
        return new GsonBuilder()
                .setFieldNamingPolicy(FieldNamingPolicy.IDENTITY)
                .create();
    }

    @Provides
    @Singleton
    OkHttpClient okHttpClient(Cache cache) {

        return new OkHttpClient.Builder()
                .cache(cache)
                .build();
    }

    @Provides
    @Singleton
    GsonConverterFactory gsonConverterFactory(Gson gson) {

        return GsonConverterFactory.create(gson);
    }

    @Provides
    @Singleton
    Retrofit retrofit(GsonConverterFactory gsonConverterFactory, OkHttpClient okHttpClient) {

        return new Retrofit.Builder()
                .baseUrl(Config.BASE_URL)
                .addConverterFactory(gsonConverterFactory)
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .client(okHttpClient)
                .build();
    }

    @Provides
    @Singleton
    IApiModule apiModule(Retrofit retrofit) {

        return retrofit.create(IApiModule.class);
    }
}
