package com.amelesh.chat10.view.activity;


import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;

import com.amelesh.chat10.R;
import com.amelesh.chat10.constants.Constants;

/**
 * Created by UserPC on 05.02.2017.
 */

public class AloneFragmentActivity extends BaseAppCompatActivity {

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        int layoutRes = R.layout.layout_content_main;

        if (getIntent() != null) {

            String fragmentName = getIntent().getStringExtra(Constants.LAYOUT_ID);
            int layoutRes2 = R.layout.layout_content_main;
            layoutRes = layoutRes2 != -1 ? layoutRes2 : layoutRes;

            setFragment(fragmentName);


        }
        setContentView(R.layout.layout_content_main);
    }

    private void setFragment(String fragmentName) {

        if (fragmentName != null) {

            try {
                Class fragmentClass = Class.forName(fragmentName);
                Fragment fragment = (Fragment) fragmentClass.newInstance();
                getSupportFragmentManager()
                        .beginTransaction().replace(R.id.frameLayoutFragment, fragment)
                        .commit();
            } catch (Exception e) {

                e.printStackTrace();
            }
        }
    }


    public static class Builder {

        private BaseAppCompatActivity context;
        private Fragment fragment;
        private Bundle bundle;
        private int requestCode = -1;
        private int layout = -1;

        public Builder(Fragment fragment) {
            this.fragment = fragment;

        }

        public Builder(BaseAppCompatActivity context) {

            this.context = context;
        }

        public Builder arrgs(Bundle bundle) {
            this.bundle = bundle;

            return this;

        }

        public void start(Class<? extends Fragment> fragmentClass) {

            Intent intent;
            Activity activity;
            if (fragment != null) {
                intent = new Intent(fragment.getContext(), AloneFragmentActivity.class);


            } else intent = new Intent(context, AloneFragmentActivity.class);
            activity = context;

            if (layout != -1) {
                intent.putExtra(Constants.LAYOUT_RES, layout);
            }

            intent.putExtra(Constants.FRAGMENT_NAME, fragmentClass.getName());

            if (requestCode != -1) {

                activity.startActivityForResult(intent, requestCode);
            } else {
                activity.startActivity(intent);
            }
        }

    }


}


