package com.amelesh.chat10.util;

/**
 * Created by UserPC on 27.02.2017.
 */

public interface Consumer<T> {

    void accept(T t);
}
